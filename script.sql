CREATE TABLE IF NOT EXISTS utilisateur(
   idUser INT primary key auto_increment NOT NULL,
   nomUtilisateur VARCHAR(50) ,
   mdpUtilisateur VARCHAR(500),
   admin BOOLEAN
);

CREATE TABLE IF NOT EXISTS personnage(
   idPersonnage INT primary key auto_increment NOT NULL,
   nomPersonnage VARCHAR(50) ,
   descriptionPersonnage VARCHAR(1000) ,
   imagePersonnage VARCHAR(50) ,
   logoPersonnage VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS acteur(
   idActeur INT primary key auto_increment NOT NULL,
   nomActeur VARCHAR(50) ,
   DescriptionActeur VARCHAR(50) ,
   imageActeur VARCHAR(50) 
);

CREATE TABLE IF NOT EXISTS categorie(
   idCategorie INT primary key auto_increment NOT NULL,
   nomCategorie VARCHAR(50) 
);

CREATE TABLE IF NOT EXISTS serie(
   idSerie INT primary key auto_increment NOT NULL,
   nomSerie VARCHAR(50) ,
   descriptionSerie VARCHAR(1000) ,
   trailerSerie VARCHAR(100) ,
   avisSerie DECIMAL(15,2)  ,
   afficheSerie VARCHAR(80) ,
   nombreSaison INT,
   dureeSerie INT,
   fondSerie VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS plateforme(
   idPlateforme INT primary key auto_increment NOT NULL,
   nomPlateforme VARCHAR(50) ,
   logoPlateforme VARCHAR(50)

);

CREATE TABLE IF NOT EXISTS posseder(
   idPersonnage INT,
   idSerie INT, 
   FOREIGN KEY(idPersonnage) REFERENCES personnage(idPersonnage),
   FOREIGN KEY(idSerie) REFERENCES serie(idSerie)
);

CREATE TABLE IF NOT EXISTS appartenir(
   idCategorie INT,
   idSerie INT,
   FOREIGN KEY(idCategorie) REFERENCES categorie(idCategorie),
   FOREIGN KEY(idSerie) REFERENCES serie(idSerie)
);

CREATE TABLE IF NOT EXISTS jouer(
   idPersonnage INT,
   idActeur INT,
   PRIMARY KEY(idPersonnage, idActeur),
   FOREIGN KEY(idPersonnage) REFERENCES personnage(idPersonnage),
   FOREIGN KEY(idActeur) REFERENCES acteur(idActeur)
);

CREATE TABLE IF NOT EXISTS presenter(
   idSerie INT,
   idPlateforme INT,
   PRIMARY KEY(idSerie, idPlateforme),
   FOREIGN KEY(idSerie) REFERENCES serie(idSerie),
   FOREIGN KEY(idPlateforme) REFERENCES plateforme(idPlateforme)
);
