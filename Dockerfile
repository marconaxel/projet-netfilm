FROM node:10-alpine

RUN mkdir -p /home/node/netfilm/node_modules && chown -R node:node /home/node/netfilm

WORKDIR /home/node/netfilm

COPY package*.json ./

USER node

RUN npm install

COPY --chown=node:node . .

EXPOSE 80 3306 443

CMD [ "node", "server.js" ]
