let mysql = require('mysql2');
let fs = require('fs');

let db = mysql.createConnection({
	host: '192.168.56.11',
    user: 'root',
    password: 'root',
    database : 'netfilm',
    port: '3306',
    multipleStatements: true
});

//script sql : creation des tables si elles n'existent pas
const createBd = fs.readFileSync('./script.sql').toString();

//script sql : ajout de données dans les tables
const initBd = fs.readFileSync('./donnees.sql').toString();

db.connect(function(err) {
    if (err) 
        throw err;
    console.log("Connecté à la base de données MySQL!");
    db.query("USE netfilm", function(err, result){
        if (err){
            db.end; // fin de connexion
            throw err;
        } else {
            db.query(createBd, (err) => {
                if (err) {
                    console.log("Problème création tables");
                } else {
                    console.log("Tables créées");
                    db.query(initBd, (err) => {
                        if (err) {
                            console.log("Problème ajout données");
                        } else {
                            console.log("Données ajoutées aux tables");
                        }
                    });
                }
            });
        }
    });
});

module.exports = {
    db,
}


//Si pb mysql executer ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password'; et flush privileges;
