let express = require('express');
var expressVue = require("express-vue");
let app = express();
let port = 8080;
var path = require('path');
var bodyParser = require('body-parser');
var session = require('express-session');
var bcrypt = require('bcrypt-nodejs');
let requetebdd = require('./public/js/requetebdd');
var multer = require('multer');
const { url } = require('inspector');

const fs = require('fs');
const http = require('http');
const https = require('https');

// Certificate
const privateKey = fs.readFileSync(__dirname + '/cert/privkey.pem', 'utf8');
const certificate = fs.readFileSync( __dirname +'/cert/cert.pem', 'utf8');
const ca = fs.readFileSync( __dirname +'/cert/chain.pem', 'utf8');

const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
};



var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        if (file.fieldname === "fondpage" || file.fieldname === "affiche") {
            cb(null, './public/img/serie');
        } else if (file.fieldname === "iconePersonnage" || file.fieldname === "fondPersonnage") {
            cb(null, './public/img/personnage')
        }
        
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname) //Appending extension
    }
  })

var upload = multer({ storage: storage });


app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

app.use(express.static('public'));
app.use('/css', express.static(__dirname + 'public/css'));
app.use('/js', express.static(__dirname + 'public/js'));
app.use('/img', express.static(__dirname + 'public/img'));

let mysql = require(__dirname + '/public/js/mysql.js');
let db = mysql.db;

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

app.use(session({
    username: '',
    connexion: false,
    admin: false,
    secret: 'testetstets',
    resave: true,
    saveUninitialized: true,
    cookie: {
        maxAge:(600000)
    }      
}));


   // methode GET
app.get('/', function(req, res){
    if (req.session.connexion == undefined) {
        req.session.connexion = false;
        req.session.admin = false;
    }
    console.log(req.session);
    res.render(__dirname + '/views/index.html', {username: req.session.username, connexion: req.session.connexion, admin: req.session.admin});
});

// methode GET
app.get('/connexion', function(req, res){
    if (req.session.connexion == undefined || req.session.connexion == false) {
        res.render(__dirname + '/views/connexion.html', {nontrouve: false});
    } else {
        res.redirect('/');
    }
});

// POST Connexion
app.post('/connexion', (req, res) => {
    if (req.body.username != undefined && req.body.username != ""
        && req.body.mdp != undefined && req.body.mdp != "") {
        var username = req.body.username;
        var mdp = req.body.mdp;
        var sql = "SELECT utilisateur.nomUtilisateur AS user_name, utilisateur.mdpUtilisateur AS user_mdp, utilisateur.admin AS user_admin"
        + " FROM utilisateur WHERE nomUtilisateur = ?";
        db.query(sql, [username], (err, result) => {
            if (err) throw err;
            if (result.length == 1 && bcrypt.compareSync(req.body.mdp, result[0].user_mdp)) {
                req.session.username = result[0].user_name;
                req.session.connexion = true;
                if (result[0].user_admin == 1) {
                    req.session.admin = true;
                }
                res.redirect('/');
            } else {
                res.render(__dirname + '/views/connexion.html', {nontrouve: true});
            }
        });
    }
});

// GET Deconnexion
app.get('/deconnexion', (req, res) => {
        req.session.connexion = false;
        req.session.admin = false;
        req.session.username = '';
        res.redirect('/');
});

// GET Inscription
app.get('/inscription', function(req, res){
    if (req.session.connexion == undefined || req.session.connexion == false) {
        res.render(__dirname + '/views/inscription.html', {existe: false});
    } else {
        res.redirect('/');
    }
});

// POST Inscription
app.post('/inscription', (req, res) => {
    if (req.body.username != undefined && req.body.username != ""
        && req.body.mdp != undefined && req.body.mdp != "") {
        var username = req.body.username;
        var mdp = req.body.mdp;
        var sqlVerif = "SELECT utilisateur.nomUtilisateur AS user_name, utilisateur.mdpUtilisateur AS user_mdp, utilisateur.admin AS user_admin"
        + " FROM utilisateur WHERE nomUtilisateur = ?";
        db.query(sqlVerif, [username], (err, result) => {
            if (err) throw err;
            if (result.length == 1 && bcrypt.compareSync(req.body.mdp, result[0].user_mdp)) {
                res.render(__dirname + '/views/inscription.html', {existe: true});
            } else {
                var sql = "INSERT INTO utilisateur (nomUtilisateur, mdpUtilisateur, admin) VALUES (?)";
                db.query(sql, [[username, bcrypt.hashSync(mdp), '1']], (err) => {
                    if (err) throw err;
                    req.session.username = req.body.username;
                    req.session.connexion = true;
                    req.session.admin=true;
                    res.redirect('/');
                });
            }
        });
    }
});

// methode GET
app.get('/ajout', function(req, res){
    console.log(req.session);
    if (req.session.admin == true) {
        var sql = requetebdd.viewNomCategorie;
        db.query(sql, (err, result) => {
            if (err) throw err;
            if (result.length > 0) {
                var nomCategories = new Array(result.length);
                for (let i = 0; i < result.length; i++) {
                    nomCategories[i] = result[i].cat_nom;
                }
                res.render(__dirname + '/views/ajout.html', {categories: nomCategories});
                console.log(nomCategories);
            } else {
                res.redirect('/');
            }
        });
    } else {
        res.redirect('/');
    }
});

//POST Ajout
app.post('/ajout', upload.fields([
    {name: 'fondpage', maxCount: 1}, 
    {name: 'affiche', maxCount: 1}, 
    {name: 'iconePersonnage', maxCount: 1},
    {name: 'fondPersonnage', maxCount: 1}
    ]), (req, res) => {
    if (!req.files) {
        console.log("fichiers non reçu");
    } else {
        console.log(req.files);
    }
    var pathFondpage = (req.files['fondpage'][0].path).replace('public', '');
    pathFondpage = pathFondpage.replaceAll('\\', '/');
    var pathAffiche = (req.files['affiche'][0].path).replace('public', '');
    pathAffiche = pathAffiche.replaceAll('\\', '/');
    var pathIconePersonnage = (req.files['iconePersonnage'][0].path).replace('public', '');
    pathIconePersonnage = pathIconePersonnage.replaceAll('\\', '/');
    var pathFondPersonnage = (req.files['fondPersonnage'][0].path).replace('public', '');
    pathFondPersonnage = pathFondPersonnage.replaceAll('\\', '/');
    
    var sqlSerie = requetebdd.ajouterSerie(req.body.nom, req.body.description, req.body.urlTrailer, 0.0, pathAffiche, 1, 0.0, pathFondpage);
    var sqlPersonnage =requetebdd.ajouterPersonnage(req.body.nomPersonnage, req.body.descriptionPersonnage, pathFondPersonnage, pathIconePersonnage);
    // ajout serie
    db.query(sqlSerie, (err) => {
        if (err) {
            console.log('problem ajout serie');
        } else {
            // ajout personnage
            db.query(sqlPersonnage, (err) => {
                if (err) {
                    console.log('problem ajout personnage');
                }
            });
        }
    });
    res.redirect('/');
});

// methode GET
app.get('/serie', function(req, res){
    res.sendFile(__dirname + '/views/serie.html');
});

// methode GET
app.get('/series.ajax', function(req, res){

    let viewSerie = requetebdd.viewSerie;
    //Affichage de toutes les séries
    db.query(viewSerie, function (err, result) {
        if (err){
            db.end; // fin de connexion
            throw err;
        }
        res.send(JSON.stringify(result));
    });
});

// methode GET
app.post('/presentation', function(req, res){
    let serie = req.body.nomSerie;
    res.render(__dirname + '/views/presentation.html', {nomSerie: serie});
});

// methode POST
app.post('/presentation.ajax', function(req, res){
    let nomSerie = req.query.nomSerie;
    let viewPresentation = requetebdd.viewPresentation(nomSerie);
    db.query(viewPresentation, function (err, result) {
        if (err){
            db.end; // fin de connexion
            throw err;
        }
        res.send(JSON.stringify(result));
    });
});

// methode POST
app.post('/trailer.ajax', function(req, res){
    let nomSerie = req.query.nomSerie;
    let viewTrailer = requetebdd.viewTrailer(nomSerie);
    db.query(viewTrailer, function (err, result) {
        if (err){
            db.end; // fin de connexion
            throw err;
        }
        res.send(JSON.stringify(result));
    });
});

app.listen(port, function(){
    console.log('Example app écoute sur le port : ' + port);
});


// Test if the request is HTTP or HTTPS. Returns true if HTTPS.
function isSecure(req) {
    if (req.headers["x-forwarded-proto"]) {
      return req.headers["x-forwarded-proto"] === "https";
    }
    return req.secure;
  }
  
  // If the request is HTTP, redirect to HTTPS.
  app.use((req, res, next) => {
    if (!isSecure(req)) {
      return res.redirect("https://" + req.headers.host + req.url);
    } else {
      next();
    }
  });


// Starting both http & https servers
const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);

httpServer.listen(80, () => {
	console.log('HTTP Server running on port 80');
});

httpsServer.listen(443, () => {
	console.log('HTTPS Server running on port 443');
});
