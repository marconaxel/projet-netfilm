INSERT INTO serie (nomSerie, descriptionSerie, trailerSerie, avisSerie, afficheSerie, nombreSaison, dureeSerie, fondSerie)
 VALUES ('ARCANE', 
 'Arcane est une série télévisée d\'animation américano-française dont le scénario prend place dans l\'univers du jeu vidéo League of Legends. Produite par Riot Games, elle est réalisée par Fortiche Production et diffusée à l\'international sur Netflix. La première saison de la série, en production depuis 2016 et disposant d\'un budget particulièrement conséquent, est diffusée tout au long du mois de novembre 2021 et est très favorablement reçue par la critique, qui acclame tout particulièrement la qualité de son animation.Dans la foulée de la diffusion de cette première saison, Riot Games en annonce la production d\'une deuxième, dont la sortie n\'est pas prévue avant 2023.', 
 'https://www.youtube.com/embed/v-dgpzE7txk', 
 4.1, 
 '/img/serie/arcane.jpg', 
 1 , 
 90 , 
 '/img/serie/arcane_wallpaper.jpg');
 
INSERT INTO personnage (nomPersonnage, descriptionPersonnage, imagePersonnage, logoPersonnage) 
VALUES ('CAITLYN', 
'Caitlyn est la plus célèbre gardienne de la paix à Piltover, mais elle est aussi la plus apte à débarrasser la ville de ses criminels les plus insaisissables. Elle fait souvent équipe avec Vi, son calme faisant contrepoids à la fougue de sa partenaire. Bien qu\'elle porte un fusil Hextech unique en son genre, la meilleure arme de Caitlyn reste son intelligence ; elle invente en effet des pièges élaborés pour attraper les bandits qui auraient l\'audace d\'agir dans la Cité du progrès.',
'/img/personnage/caitlyn-fond.jpg',
'/img/personnage/caitlyn_logo.jpg');

INSERT INTO personnage (nomPersonnage, descriptionPersonnage, imagePersonnage, logoPersonnage) 
VALUES ('JAYCE', 
'Jayce est un brillant inventeur qui a voué sa vie à la défense de Piltover et de sa poursuite inlassable du progrès. Armé de son marteau Hextech polymorphe, Jayce utilise aussi bien sa force que son courage et son intelligence pour protéger sa ville. Bien que considéré comme un héros par ses compatriotes, il n\'apprécie pas vraiment l\'attention que cela lui vaut. Jayce a néanmoins un cœur d\'or, et même ceux qui sont jaloux de ses talents naturels lui sont reconnaissants de protéger la Cité du progrès.',
'/img/personnage/jayce-fond.jpg',
'/img/personnage/jayce_logo.jpg');

INSERT INTO personnage (nomPersonnage, descriptionPersonnage, imagePersonnage, logoPersonnage) 
VALUES ('JINX', 
'Criminelle hystérique et impulsive de Zaun, Jinx ne vit que pour semer le chaos sans se préoccuper des conséquences. Équipée d\'un arsenal d\'armes mortelles, elle déchaîne les explosions les plus spectaculaires en ne laissant dans son sillage que destruction et panique. Jinx déteste s\'ennuyer et c\'est dans la bonne humeur qu\'elle sème le chaos dans tous les endroits qu\'elle traverse.',
'/img/personnage/jinx-fond.jpg',
'/img/personnage/jinx_logo.jpg');

INSERT INTO personnage (nomPersonnage, descriptionPersonnage, imagePersonnage, logoPersonnage) 
VALUES ('MEL', 
'Mel Medarda est une Aristocrate noxien basée à Piltover et membre du conseil dirigeant de Piltover. Héritière désavouée du Medarda Clan, Mel est une politicienne ambitieuse prête à tout pour se hisser au sommet. Utilisant ses prouesses politiques, les actions qu\'elle a faites ont changé à jamais le cours de l\'histoire de Piltover.',
'/img/personnage/mel-fond.jpg',
'/img/personnage/mel_logo.jpg');

INSERT INTO personnage (nomPersonnage, descriptionPersonnage, imagePersonnage, logoPersonnage) 
VALUES ('SILCO', 
'Silco était un baron de la drogue et un industriel de et le père adoptif de Jinx. Individu tyrannique et mégalomane, il était un partisan radical de l\'indépendance de Zaun vis-à-vis de Piltover et était prêt à tout pour y parvenir.',
'/img/personnage/silco-fond.jpg',
'/img/personnage/silco_logo.jpg');

INSERT INTO personnage (nomPersonnage, descriptionPersonnage, imagePersonnage, logoPersonnage) 
VALUES ('VANDER', 
'Vander était un barman Zaunite et le père adoptif de Vi et Jinx. Homme désintéressé et généreux avec un talent pour le combat au poing, il était largement respecté par les habitants de la sous-ville et était prêt à mourir pour ses propres convictions, surtout si cela signifiait sauver ceux qu\'il aimait.',
'/img/personnage/vander-fond.jpg',
'/img/personnage/vander_logo.jpg');

INSERT INTO personnage (nomPersonnage, descriptionPersonnage, imagePersonnage, logoPersonnage) 
VALUES ('VI', 
'Autrefois criminelle des quartiers louches de Zaun, Vi est une femme sans peur, mais pas sans reproche, au tempérament bouillant, impulsive, qui n\'a qu\'un respect mesuré pour les représentants de l\'autorité. Vi a grandi seule et a développé d\'excellents instincts de survie ainsi qu\'un sens de l\'humour assez caustique. Aujourd\'hui, elle travaille avec les gendarmes de Piltover pour maintenir l\'ordre et se sert pour cela de puissants gantelets Hextech qui n\'ont peur de frapper ni les plus épais murs de briques ni les suspects.',
'/img/personnage/vi-fond.jpg',
'/img/personnage/vi_logo.jpg');

INSERT INTO personnage (nomPersonnage, descriptionPersonnage, imagePersonnage, logoPersonnage) 
VALUES ('VIKTOR', 
'Héraut d\'un nouvel âge de technologie, Viktor a consacré sa vie aux progrès de l\'humanité. Idéaliste cherchant à hisser le peuple de Zaun à de nouveaux sommets de compréhension, il pense que le potentiel de l\'humanité ne pourra être réalisé que par le grand pas en avant d\'une glorieuse évolution technologique. Doté d\'un corps optimisé par l\'acier et la science, Viktor poursuit fanatiquement son idéal d\'un meilleur avenir.',
'/img/personnage/viktor-fond.jpg',
'/img/personnage/viktor_logo.jpg');

INSERT INTO categorie (nomCategorie) VALUES ('Animation');

INSERT INTO categorie (nomCategorie) VALUES ('Action');

INSERT INTO categorie (nomCategorie) VALUES ('Aventure');

INSERT INTO posseder (idPersonnage, idSerie ) VALUE(1, 1);

INSERT INTO posseder (idPersonnage, idSerie ) VALUE(2, 1);

INSERT INTO posseder (idPersonnage, idSerie ) VALUE(3, 1);

INSERT INTO posseder (idPersonnage, idSerie ) VALUE(4, 1);

INSERT INTO posseder (idPersonnage, idSerie ) VALUE(5, 1);

INSERT INTO posseder (idPersonnage, idSerie ) VALUE(6, 1);

INSERT INTO posseder (idPersonnage, idSerie ) VALUE(7, 1);

INSERT INTO posseder (idPersonnage, idSerie ) VALUE(8, 1);

INSERT INTO appartenir (idCategorie, idSerie ) VALUE(1, 1);

INSERT INTO appartenir (idCategorie, idSerie ) VALUE(2, 1);

INSERT INTO appartenir (idCategorie, idSerie ) VALUE(3, 1);

INSERT INTO serie (nomSerie, descriptionSerie, trailerSerie, avisSerie, afficheSerie, nombreSaison, dureeSerie, fondSerie)
 VALUES ('DAREVIL', 
 'Arcane est une série télévisée d\'animation américano-française dont le scénario prend place dans l\'univers du jeu vidéo League of Legends. Produite par Riot Games, elle est réalisée par Fortiche Production et diffusée à l\'international sur Netflix. La première saison de la série, en production depuis 2016 et disposant d\'un budget particulièrement conséquent, est diffusée tout au long du mois de novembre 2021 et est très favorablement reçue par la critique, qui acclame tout particulièrement la qualité de son animation.Dans la foulée de la diffusion de cette première saison, Riot Games en annonce la production d\'une deuxième, dont la sortie n\'est pas prévue avant 2023.', 
 'https://www.youtube.com/embed/v-dgpzE7txk', 
 4.1, 
 '/img/serie/darevil.jpg', 
 1 , 
 90 , 
 '/img/serie/arcane_wallpaper.jpg');

INSERT INTO serie (nomSerie, descriptionSerie, trailerSerie, avisSerie, afficheSerie, nombreSaison, dureeSerie, fondSerie)
 VALUES ('FLASH', 
 'Arcane est une série télévisée d\'animation américano-française dont le scénario prend place dans l\'univers du jeu vidéo League of Legends. Produite par Riot Games, elle est réalisée par Fortiche Production et diffusée à l\'international sur Netflix. La première saison de la série, en production depuis 2016 et disposant d\'un budget particulièrement conséquent, est diffusée tout au long du mois de novembre 2021 et est très favorablement reçue par la critique, qui acclame tout particulièrement la qualité de son animation.Dans la foulée de la diffusion de cette première saison, Riot Games en annonce la production d\'une deuxième, dont la sortie n\'est pas prévue avant 2023.', 
 'https://www.youtube.com/embed/v-dgpzE7txk', 
 4.1, 
 '/img/serie/flash.jpg', 
 1 , 
 90 , 
 '/img/serie/arcane_wallpaper.jpg');

INSERT INTO serie (nomSerie, descriptionSerie, trailerSerie, avisSerie, afficheSerie, nombreSaison, dureeSerie, fondSerie)
VALUES ('GAME OF THRONES', 
'Arcane est une série télévisée d\'animation américano-française dont le scénario prend place dans l\'univers du jeu vidéo League of Legends. Produite par Riot Games, elle est réalisée par Fortiche Production et diffusée à l\'international sur Netflix. La première saison de la série, en production depuis 2016 et disposant d\'un budget particulièrement conséquent, est diffusée tout au long du mois de novembre 2021 et est très favorablement reçue par la critique, qui acclame tout particulièrement la qualité de son animation.Dans la foulée de la diffusion de cette première saison, Riot Games en annonce la production d\'une deuxième, dont la sortie n\'est pas prévue avant 2023.', 
'https://www.youtube.com/embed/v-dgpzE7txk', 
4.1, 
'/img/serie/game-of-thrones.jpg', 
1 , 
90 , 
'/img/serie/arcane_wallpaper.jpg');

INSERT INTO serie (nomSerie, descriptionSerie, trailerSerie, avisSerie, afficheSerie, nombreSaison, dureeSerie, fondSerie)
VALUES ('HOW I MET YOUR MOTHER', 
'Arcane est une série télévisée d\'animation américano-française dont le scénario prend place dans l\'univers du jeu vidéo League of Legends. Produite par Riot Games, elle est réalisée par Fortiche Production et diffusée à l\'international sur Netflix. La première saison de la série, en production depuis 2016 et disposant d\'un budget particulièrement conséquent, est diffusée tout au long du mois de novembre 2021 et est très favorablement reçue par la critique, qui acclame tout particulièrement la qualité de son animation.Dans la foulée de la diffusion de cette première saison, Riot Games en annonce la production d\'une deuxième, dont la sortie n\'est pas prévue avant 2023.', 
'https://www.youtube.com/embed/v-dgpzE7txk', 
4.1, 
'/img/serie/how-i-met-your-mother.jpg', 
1 , 
90 , 
'/img/serie/arcane_wallpaper.jpg');

INSERT INTO serie (nomSerie, descriptionSerie, trailerSerie, avisSerie, afficheSerie, nombreSaison, dureeSerie, fondSerie)
VALUES ('RAGNAROK', 
'Arcane est une série télévisée d\'animation américano-française dont le scénario prend place dans l\'univers du jeu vidéo League of Legends. Produite par Riot Games, elle est réalisée par Fortiche Production et diffusée à l\'international sur Netflix. La première saison de la série, en production depuis 2016 et disposant d\'un budget particulièrement conséquent, est diffusée tout au long du mois de novembre 2021 et est très favorablement reçue par la critique, qui acclame tout particulièrement la qualité de son animation.Dans la foulée de la diffusion de cette première saison, Riot Games en annonce la production d\'une deuxième, dont la sortie n\'est pas prévue avant 2023.', 
'https://www.youtube.com/embed/v-dgpzE7txk', 
4.1, 
'/img/serie/ragnarok.jpg', 
1 , 
90 , 
'/img/serie/arcane_wallpaper.jpg');

INSERT INTO serie (nomSerie, descriptionSerie, trailerSerie, avisSerie, afficheSerie, nombreSaison, dureeSerie, fondSerie)
VALUES ('THE WITCHER', 
'Arcane est une série télévisée d\'animation américano-française dont le scénario prend place dans l\'univers du jeu vidéo League of Legends. Produite par Riot Games, elle est réalisée par Fortiche Production et diffusée à l\'international sur Netflix. La première saison de la série, en production depuis 2016 et disposant d\'un budget particulièrement conséquent, est diffusée tout au long du mois de novembre 2021 et est très favorablement reçue par la critique, qui acclame tout particulièrement la qualité de son animation.Dans la foulée de la diffusion de cette première saison, Riot Games en annonce la production d\'une deuxième, dont la sortie n\'est pas prévue avant 2023.', 
'https://www.youtube.com/embed/v-dgpzE7txk', 
4.1, 
'/img/serie/the-witcher.jpg', 
1 , 
90 , 
'/img/serie/arcane_wallpaper.jpg');

INSERT INTO serie (nomSerie, descriptionSerie, trailerSerie, avisSerie, afficheSerie, nombreSaison, dureeSerie, fondSerie)
VALUES ('THE WHEEL OF TIME', 
'Arcane est une série télévisée d\'animation américano-française dont le scénario prend place dans l\'univers du jeu vidéo League of Legends. Produite par Riot Games, elle est réalisée par Fortiche Production et diffusée à l\'international sur Netflix. La première saison de la série, en production depuis 2016 et disposant d\'un budget particulièrement conséquent, est diffusée tout au long du mois de novembre 2021 et est très favorablement reçue par la critique, qui acclame tout particulièrement la qualité de son animation.Dans la foulée de la diffusion de cette première saison, Riot Games en annonce la production d\'une deuxième, dont la sortie n\'est pas prévue avant 2023.', 
'https://www.youtube.com/embed/v-dgpzE7txk', 
4.1, 
'/img/serie/the-wheel-of-time.jpg', 
1 , 
90 , 
'/img/serie/arcane_wallpaper.jpg');
